package reader.mf.pi.com.manifestreader.manifestutil;

import android.content.res.Resources;
import android.content.res.XmlResourceParser;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * Utility class to read and build XML text from xml resource parser.
 *
 * Created by peeyushpathak on 30/05/18.
 */
public class XMLReaderUtil {


    public static final char NEW_LINE_CHAR = '\n';
    public static final char START_TAG_CHAR = '<';
    public static final char START_END_TAG_CLOSE_CHAR = '>';
    public static final String END_TAG_START_CHAR = "</";
    public static final String COMMENT_START = "<!--";
    public static final String COMMENT_END = "-->";
    public static final String CDATA_START_TAG = "<!CDATA[";
    public static final String CDATA_END_TAG = "]]>";
    public static final String PROCESSING_START_TAG = ">?";
    public static final String PROCESSING_END_TAG = "?>";



    /**
     * This method is the utility function used to read xml file using
     * XmlResourceParser and Resources.
     * Resources will be used to map the id with resources provided
     *
     * @param xrp
     * @param currentResources
     * @return CharSequence
     * @throws XmlPullParserException
     * @throws IOException
     */
    public static CharSequence getXMLText(XmlResourceParser xrp,
                                          Resources currentResources) throws XmlPullParserException, IOException {
        StringBuffer sb = new StringBuffer();
        int indent = 0;
            int eventType = xrp.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                // for sb
                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        indent += 1;
                        sb.append(NEW_LINE_CHAR);
                        insertSpaces(sb, indent);
                        sb.append(START_TAG_CHAR).append(xrp.getName());
                        sb.append(getAttributes(xrp, currentResources));
                        sb.append(START_END_TAG_CLOSE_CHAR);
                        break;
                    case XmlPullParser.END_TAG:
                        indent -= 1;
                        sb.append(NEW_LINE_CHAR).append(NEW_LINE_CHAR);
                        insertSpaces(sb, indent);
                        sb.append(END_TAG_START_CHAR).append(xrp.getName()).append(START_END_TAG_CLOSE_CHAR);
                        break;

                    case XmlPullParser.TEXT:
                        sb.append(xrp.getText());
                        break;

                    case XmlPullParser.CDSECT:
                        sb.append(CDATA_START_TAG).append(xrp.getText()).append(CDATA_END_TAG);
                        break;

                    case XmlPullParser.PROCESSING_INSTRUCTION:
                        sb.append(PROCESSING_START_TAG).append(xrp.getText()).append(PROCESSING_END_TAG);
                        break;

                    case XmlPullParser.COMMENT:
                        sb.append(COMMENT_START).append(xrp.getText()).append(COMMENT_END);
                        break;
                }
                eventType = xrp.nextToken();
            }

        return sb;
    }

    /**
     * add space in string buffer
     *
     * @param sb
     * @param num
     */
    public static  void insertSpaces(StringBuffer sb, int num) {
        if (sb == null)
            return;
        for (int i = 0; i < num; i++)
            sb.append(" ");
    }


    /**
     * returns the string by resolving it through the resources if it
     * has a resource ID. Otherwise just returns what was provided.
     *
     * @param in
     *            String to resolve
     * @param resources
     *            Context appropriate resource (system for system, package's for
     *            package)
     * @return Resolved value, either the input, or some other string.
     */
    public static String resolveValue(String in, Resources resources) {
        if (in == null || !in.startsWith("@") || resources == null)
            return in;
        try {
            int num = Integer.parseInt(in.substring(1));
            return resources.getString(num);
        } catch (NumberFormatException e) {
            return in;
        } catch (RuntimeException e) {
            return in;
        }
    }

    /**
     * This method will get the content of particular tag name
     *
     * @param xrp
     * @param currentResources
     * @return CharSequence
     */
    public static CharSequence getAttributes(XmlResourceParser xrp,
                                    Resources currentResources) {
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < xrp.getAttributeCount(); i++)
            sb.append(NEW_LINE_CHAR).append(xrp.getAttributeName(i)).append("=\"").append(resolveValue(xrp.getAttributeValue(i), currentResources)).append("\"");
        return sb;

    }
}
