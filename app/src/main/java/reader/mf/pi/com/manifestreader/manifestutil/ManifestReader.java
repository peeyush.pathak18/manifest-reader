package reader.mf.pi.com.manifestreader.manifestutil;

import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.content.res.XmlResourceParser;
import android.util.Log;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

/**
 * This class is responsible for Reading the manifest of particular package name
 * Created by peeyushpathak on 30/05/18.
 */
public class ManifestReader {

    public static final String MANIFEST_FILE_NAME = "AndroidManifest.xml";


    private String packageName;
    private AssetManager assetManager;
    private Resources resources;



    public ManifestReader(Context context, String packageId){
        this.packageName = packageId;
        this.assetManager = getAssetManagerFor(context , packageId);
        this.resources = context.getResources();
    }



    public AssetManager getAssetManagerFor(Context context, String  packageName){
        AssetManager assetManager = null;
        try {
            assetManager = context.createPackageContext(packageName, Context.CONTEXT_RESTRICTED).getAssets();
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("ManifestReader", "Problem finding asset manager for "+packageName);
        }
        return assetManager;
    }

    /**
     * Reads and returns manifest text
     *
     * @return CharSequence
     * @throws IOException
     * @throws XmlPullParserException
     * @throws NullPointerException
     */
    public CharSequence readManifest() throws IOException, XmlPullParserException,NullPointerException {
        XmlResourceParser xrp = assetManager.openXmlResourceParser(MANIFEST_FILE_NAME);
        return XMLReaderUtil.getXMLText(xrp, resources);
    }

}
