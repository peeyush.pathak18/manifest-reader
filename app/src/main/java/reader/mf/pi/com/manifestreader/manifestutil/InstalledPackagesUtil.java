package reader.mf.pi.com.manifestreader.manifestutil;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.pm.ServiceInfo;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by peeyushpathak on 28/05/18.
 * Utility class for reading the Manifest
 */
public class InstalledPackagesUtil {


    /**
     * Gets the installed packages on the device
     * @param context
     * @return ArrayList<InstalledPackage>
     */
    public static ArrayList<InstalledPackage> getInstalledPackages(@NonNull Context context){
        ArrayList<InstalledPackage> installedPackageList = new ArrayList<>();
        PackageManager packageManager = context.getPackageManager();

        Intent intent = new Intent(Intent.ACTION_MAIN,null);
        intent.addCategory(Intent.CATEGORY_LAUNCHER);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                |Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);

        // Initialize a new list of resolve info which will handle the above created intent
        List<ResolveInfo> resolveInfoList = context.getPackageManager().queryIntentActivities(intent,0);

        for(ResolveInfo resolveInfo : resolveInfoList){
            // Get the activity info from resolve info
            ActivityInfo activityInfo = resolveInfo.activityInfo;

            // Get the package name and application label
            String packageName = activityInfo.applicationInfo.packageName;
            String label = (String) packageManager.getApplicationLabel(activityInfo.applicationInfo);

            installedPackageList.add(new InstalledPackage(packageName, label));
        }
        return installedPackageList;
    }


    /**
     * Get a list of granted permissions for the given package name
     * @param context
     * @param packageName
     * @return ArrayList<String>
     */
    public static ArrayList<String> getGrantedPermissionList(@NonNull Context context,@NonNull String packageName){
        ArrayList<String> permissionList = new ArrayList<>();

        // Get the package info
        PackageInfo packageInfo = null;
        try {
            packageInfo = context.getPackageManager().getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
            //add the granted permissions to the list
            for (int i = 0; i < packageInfo.requestedPermissions.length; i++) {
                if ((packageInfo.requestedPermissionsFlags[i] & PackageInfo.REQUESTED_PERMISSION_GRANTED) != 0) {
                    String permission =packageInfo.requestedPermissions[i];
                    permissionList.add(permission);
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return permissionList;
    }

    /**
     * Get the array of PackageInfo for the given package name
     * @param context
     * @param packageName
     * @param getFlag
     * @return PackageInfo
     */
    public static PackageInfo getPackageInfo(Context context, String packageName, int getFlag){
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageName, getFlag);
            return info;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    /**
     * Get the array of ActivityInfo for the given package name
     * @param context
     * @param packageName
     * @return ActivityInfo[]
     */
    public static ActivityInfo[] getActivitiesFor(Context context, String packageName){
        StringBuilder builder = new StringBuilder();
        PackageInfo packageInfo = getPackageInfo(context, packageName, PackageManager.GET_ACTIVITIES);
        if(packageInfo==null){
            return new ActivityInfo[]{};
        }

        ActivityInfo[] activityInfos = packageInfo.activities;

        if(activityInfos != null)
            return activityInfos;
        else
            return new ActivityInfo[]{};

    }


    /**
     * Get the array of ActivityInfo for receivers for the given package name
     * @param context
     * @param packageName
     * @return ActivityInfo[]
     */
    public static ActivityInfo[] getReceiverFor(Context context, String packageName){
        StringBuilder builder = new StringBuilder();
        PackageInfo packageInfo = getPackageInfo(context, packageName, PackageManager.GET_RECEIVERS);
        if(packageInfo==null){
            return new ActivityInfo[]{};
        }

        ActivityInfo[] activityInfos = packageInfo.receivers;

        if(activityInfos != null)
            return new ActivityInfo[]{};
        else
            return activityInfos;
    }

    /**
     * Get the array of ServiceInfo for the given package name
     * @param context
     * @param packageName
     * @return ServiceInfo[]
     */
    public static ServiceInfo[] getServiceInfoFor(Context context, String packageName){
        StringBuilder builder = new StringBuilder();
        PackageInfo packageInfo = getPackageInfo(context, packageName, PackageManager.GET_SERVICES);
        if(packageInfo==null){
            return new ServiceInfo[]{};
        }

        ServiceInfo[] serviceInfos = packageInfo.services;

        if(serviceInfos==null)
            return new ServiceInfo[]{};
        else
            return serviceInfos;

    }

    /**
     * Get the package information for the given package ID
     * @param context
     * @param packageId
     * @return InstalledPackageInfo
     */
    public static InstalledPackageInfo readPackageInfo(@NonNull Context context,@NonNull String  packageId){
        InstalledPackageInfo installedPackageinfo = new InstalledPackageInfo(packageId);
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(packageId, PackageManager.GET_CONFIGURATIONS);
            installedPackageinfo.setGeneralInfo(info);

            installedPackageinfo.setGrantedPermission(getGrantedPermissionList(context, packageId));
            installedPackageinfo.setActivityInfos(getActivitiesFor(context, packageId));
            installedPackageinfo.setReceiverInfos(getReceiverFor(context, packageId));
            installedPackageinfo.setServiceInfos(getServiceInfoFor(context, packageId));

        } catch (Exception e) {
        }
        return installedPackageinfo;
    }
}
