package reader.mf.pi.com.manifestreader;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;

import reader.mf.pi.com.manifestreader.manifestutil.InstalledPackage;
import reader.mf.pi.com.manifestreader.manifestutil.InstalledPackagesUtil;
import reader.mf.pi.com.manifestreader.manifestutil.ManifestReader;

public class MainActivity extends AppCompatActivity {


    Spinner spAppSelection;
    TextView tvAppPackageInfo;
    ArrayList<InstalledPackage> installedApps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spAppSelection = (Spinner) findViewById(R.id.sp_package_selection);
        tvAppPackageInfo = (TextView) findViewById(R.id.tv_package_info);

        tvAppPackageInfo.setMovementMethod(new ScrollingMovementMethod());

    }


    @Override
    protected void onStart() {
        super.onStart();
        updateInstalledAppSelection();
    }



    private void updateInstalledAppSelection() {
        installedApps = InstalledPackagesUtil.getInstalledPackages(this);

        ArrayAdapter<InstalledPackage> adapter = new ArrayAdapter<InstalledPackage>(
                this,
                android.R.layout.simple_spinner_item,
                installedApps
        );


        // Set the drop down to view and select the installed packages
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spAppSelection.setAdapter(adapter);
        spAppSelection.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                updateTextFor(installedApps.get(i));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void updateTextFor(InstalledPackage installedPackage){
        ManifestReader manifestReader=  new ManifestReader(getApplicationContext() ,installedPackage.getPackageName());

        try {
            tvAppPackageInfo.setText(manifestReader.readManifest());
        } catch (IOException e) {
            showError("updateTextFor", e);
        } catch (XmlPullParserException e) {
            showError("parse error", e);
        }catch (NullPointerException e){
            showError("Problem reading manifest xml", e);
        }
    }


    protected void showError(CharSequence text, Throwable t) {
        Log.e("Manifest Reader", text + " : "
                + ((t != null) ? t.getMessage() : ""));
        Toast.makeText(this, "Error: " + text + " : " + t, Toast.LENGTH_LONG)
                .show();
    }




}
