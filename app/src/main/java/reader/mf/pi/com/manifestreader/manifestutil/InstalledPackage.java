package reader.mf.pi.com.manifestreader.manifestutil;

/**
 * Created by peeyushpathak on 28/05/18.
 */
public class InstalledPackage {
    public String packageName;
    public String appLabel;


    public InstalledPackage() {
        this.packageName = "";
        this.appLabel = "";
    }

    public InstalledPackage(String packageName, String appLabel) {
        this.packageName = packageName;
        this.appLabel = appLabel;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppLabel() {
        return appLabel;
    }

    public void setAppLabel(String appLabel) {
        this.appLabel = appLabel;
    }

    @Override
    public String toString() {
        return packageName+" : "+appLabel;
    }
}
