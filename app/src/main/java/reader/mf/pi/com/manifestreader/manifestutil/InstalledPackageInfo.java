package reader.mf.pi.com.manifestreader.manifestutil;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageInfo;
import android.content.pm.ServiceInfo;

import java.util.ArrayList;

/**
 * Created by peeyushpathak on 29/05/18.
 */
public class InstalledPackageInfo {


    String packageName;
    public ActivityInfo[] activityInfos;
    public ActivityInfo[] receiverInfos;
    public ServiceInfo[] serviceInfos;
    public ArrayList<String> grantedPermission;
    public PackageInfo generalInfo;

    public InstalledPackageInfo() {
    }

    public InstalledPackageInfo(String packageId) {
        this.packageName = packageId;
    }


    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public ActivityInfo[] getActivityInfos() {
        return activityInfos;
    }

    public void setActivityInfos(ActivityInfo[] activityInfos) {
        this.activityInfos = activityInfos;
    }

    public ActivityInfo[] getReceiverInfos() {
        return receiverInfos;
    }

    public void setReceiverInfos(ActivityInfo[] receiverInfos) {
        this.receiverInfos = receiverInfos;
    }

    public ArrayList<String> getGrantedPermission() {
        return grantedPermission;
    }

    public void setGrantedPermission(ArrayList<String> grantedPermission) {
        this.grantedPermission = grantedPermission;
    }

    public PackageInfo getGeneralInfo() {
        return generalInfo;
    }

    public void setGeneralInfo(PackageInfo generalInfo) {
        this.generalInfo = generalInfo;
    }

    public ServiceInfo[] getServiceInfos() {
        return serviceInfos;
    }

    public void setServiceInfos(ServiceInfo[] serviceInfos) {
        this.serviceInfos = serviceInfos;
    }
}
